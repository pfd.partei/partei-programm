# Umwelt

* Nachhaltige Entwicklung
* Geplante Obsoleszenz

## Quellen:
* http://taz.de/Geplante-Obsoleszenz-in-Geraeten/!5053262/
* https://www.umweltbundesamt.de/themen/wie-der-klimawandel-indirekt-die-deutsche
* https://de.wikipedia.org/wiki/Nachhaltige_Entwicklung
* http://www.bpb.de/izpb/8983/leitbild-der-nachhaltigen-entwicklung?p=all
* https://www.cairn.info/revue-l-europe-en-formation-2009-2-page-157.htm
* https://sweden.se/nature/7-examples-of-sustainability-in-sweden/
