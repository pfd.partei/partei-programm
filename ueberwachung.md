# Überwachung

 Staatliche Überwachung darf unserer Überzeugung nach erst dann einsetzen, wenn ein konkreter Verdacht gegen eine  Person vorliegt und kann dann immer nur anlassbezogen statt finden. Wir fordern zu diesem
Grundsatz unseres Rechtssystems zurückzukehren.

Durch eine drohende Überwachung staatlicher Einrichtungen kommt es zum sogenannten Chilling Effekt,
das heißt Personen verhalten sich entsprechend den vermutet erwünschten Sozialnormen[1](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2769645). Wir glauben jedoch,
dass dies für eine Demokratie extrem schädlich und kontraproduktiv ist.

Wir lehnen Überwachung sowohl im digitalen wie auch in der öffentlichen Sphäre ab, da es sich
hierbei um einen Grundrechtsabbau handelt.
Derzeitig finden Pilotprojekte zu Kameraüberwachung in Kombination mit Gesichtserkennung statt, wie sie am
 Berlin Südkreuz erprobt werden. Diese widersprechen nicht nur der aktuellen Gesetzeslage sondern sind
 erweisen sich auch als völlig ineffektiv (Erkennungsrate von 70% bei Idealbedingungen und vorausgewählten Personen).
Ein Zwischenbericht des Bundespolizeipräsidiums zum Südkreuz-Test ist nach Angaben des BMI Verschlusssache und kann nicht herausgegeben werden.[2](https://fragdenstaat.de/anfrage/zwischenergebnisse-sudkreuz-test/)
Die Möglichkeit der Überprüfung der Ergebnisse und des öffentlichen Diskurses wäre unserer Ansicht nach
notwendig für eine aufgeklärte Gesellschaft.

In Kombination u.a. mit dem neuen Personalausweisgesetz, das den Zugriff auf alle Passfotos
ermöglicht, ist es möglich ein Bewegungsprofil aller Bundesbürger zu erstellen.
Dies entspricht unserer Meinung nach nicht dem verhalten von demokratischen Staaten.
(Der verkauf von Software an Unrechtsregime muss ebenfalls stärker kontrolliert werden)

Der Staat will auch im Internet die Möglichkeit haben, seine Bürger zu überwachen.
Dies wird duch den Einsatz von sogenannten Staatstrojanern realisiert.
Dabei muss man sich aber vor Augen halten, dass dies nur unter Ausnützung von Sicherheitslücken
in Geräten möglich ist. Dieser Sicherheitslücken sollen in Zukunft von staatlichen Einrichtungen
wie [Zitis](https://www.zeit.de/digital/datenschutz/2016-06/verschluesselung-kommunikation-entschluesselung-bundesregierung-zentrale-stelle-fuer-informationstechnik-im-sicherheitsbereich) aufgespürt werden.
Durch das geheimhalten von Sicherheitslücken werden Sicherheitslücken über Jahre offen gehalten.
Diese stehen so zu einer gewissen Wahrscheinlichkeit nicht nur unseren sondern auch
ausländischen Geheimdiensten zur Verfügung. Dies öffnet zudem auch Tür und Tor für
Industriespionage.
Auch militärisch sollen Sicherheitslücken u.a. von kritischer Infrastruktur ausgenützt werden,
damit wird wissentlich die Gesundheit und Sicherheit der Gesellschaft aufs Spiel gesetzt.
(TODO: Argument nur Verteidigung)

Die Aufgabe von staatlichen Einrichtungen muss es sein, Sicherheitslücken aufzuklären und
schnellstmöglich für deren Schließung zu sorgen. Damit kann die Sicherheit der Gesellschaft
nachhaltig gesichert werden.

Es ist vielmehr Aufgabe eines Staates diese Eingriffe zu unterbinden ([Art. 8 EMRK](https://www.admin.ch/opc/de/classified-compilation/19500267/index.html#a8), [Art. 17 UNO-Pakt II](https://www.admin.ch/opc/de/classified-compilation/19660262/index.html#a17))
Deshlab ist es aus unserer Sicht notwendig ein Wettrüsten in diesem Bereich zu unterbinden und
ausländische Versuche durch erhöhte Sicherheit entgegenzuwirken.



* Kameraüberwachung
* e-ID
* Gesichterskennung
* Regulierung der Geheimdienste
* Kein staatliches Hacking

## Quellen:
* http://www.sueddeutsche.de/digital/interview-am-morgen-es-gibt-keinen-raum-mehr-in-dem-wir-nicht-ueberwacht-werden-1.3808023
* http://www.sueddeutsche.de/digital/soziale-kontrolle-in-china-menschen-und-unternehmen-bekommen-noch-mehr-macht-ihr-leben-zu-zerstoeren-1.3808187
* http://www.sueddeutsche.de/news/politik/datenschutz-polizei-setzt-immer-mehr-auf-automatische-gesichtserkennung-dpa.urn-newsml-dpa-com-20090101-180321-99-571216
* https://digitalcourage.de/blog/2018/staatstrojaner-warnbrief-an-bundestag-wortlaut
* https://www.heise.de/newsticker/meldung/Hack-Back-Bundesregierung-prueft-Moeglichkeit-von-Cybergegenangriffen-3998016.html
* https://www.zeit.de/digital/datenschutz/2016-11/verschluesselung-bnd-whatsapp-aniski-150-millionen
