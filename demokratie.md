# Stärkung Demokratie
* Gesetze auf Wirksamkeit überprüfen
* Ende der Überwachung (freie Meinungsbildung)

## Quellen:
* http://www.spiegel.de/netzwelt/netzpolitik/deutschland-schleichend-zum-ueberwachungsstaat-a-1121162.html
