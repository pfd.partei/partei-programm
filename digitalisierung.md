# Digitalisierung

* Grundeinkommen
* Besteuerung von Maschinen (Standortprinzip)
* Arbeitslosigkeit AI
* Regulierung von Algorithmen
* Pflicht zur Verwendung von OpenSource im öffentlichen Bereich

## Quellen:
* http://www.business-punk.com/2018/02/bedingungsloses-grundeinkommen-maschinen-steuer/
* http://www.business-punk.com/2017/04/automatisierung-ist-okay/
* http://www.rilke.de/gedichte/der_sonette_zweiter_teil_x.htm
* http://www.faz.net/aktuell/feuilleton/debatten/automatisierungsdividende-fuer-alle-roboter-muessen-unsere-rente-sichern-11754772.html?printPagedArticle=true#pageIndex_2
* https://www.egovernment-computing.de/open-source-heisst-zukunft-a-622042/
